const express = require('express');
const TaskController = require('../controllers/taskController.js')
const router = express.Router()



// routes

router.get('/', (req,res) => {
  TaskController.getAllTasks().then((resultFromController) => res.send(resultFromController))
})

router.put('/:id/update',(req,res) => {
  TaskController.updateTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
})

router.put('/:id/delete',(req,res) => {
  TaskController.deleteTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
})

router.post('/create',(req, res) =>{
  TaskController.createTask(req.body).then((resultFromController) => {res.send(resultFromController)}) 
})

 
module.exports = router