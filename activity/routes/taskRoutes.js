const express = require('express');
const TaskController = require('../controllers/taskController.js')
const router = express.Router()



// routes

// router.get('/', (req,res) => {
//   TaskController.getAllTasks().then((resultFromController) => res.send(resultFromController))
// })
router.post('/create',(req, res) =>{
  TaskController.createTask(req.body).then((resultFromController) => {res.send(resultFromController)}) 
})

router.get('/:id/',(req,res) => {
  TaskController.getTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
})

router.put('/update/:id',(req,res) => {
  TaskController.updateTask(req.params.id, req.body).then((resultFromController) => res.send(resultFromController))
})




 
module.exports = router