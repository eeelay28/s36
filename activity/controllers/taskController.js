const Task = require('../models/Task.js');

module.exports.getAllTasks = () => {
  // business logic

  return Task.find({}).then(result => {
      return result
  })
}

module.exports.getTask = (taskId) => {
    return Task.findById(taskId).then((foundTask, error) => {
        if (error) {
            console.log(error)
            return false
        } else {
            return foundTask;
        }
    })
};


module.exports.createTask = (reqBody) => {
  let newTask = new Task({
    name: reqBody.name
  })

  return newTask.save().then((savedTask,error) => {
    if (error){
        console.log(error)
        return false
    } else if (savedTask != null && savedTask.name==reqBody.name){
        return 'Successfully Inserted'
    }else{
      return savedTask
    }
  })
}

module.exports.updateTask = (taskId, newContent) => {
  return Task.findById(taskId).then((result, error) => {
    if (error){
      console.log(error)
      return false
    } else {
      result.status = newContent.status
      return result.save().then((updatedTask, error) => {
        if (error){
          console.log(error)
          return false
        }else{
          return updatedTask;
        }

      })
    }
  })
};


